######### Day2 ##########
###########
# NUMBER SYSTEM
## CRUD
##############
# CREATE
a=23
b=23.45
c=23+45j
### READ
# print(a,b,c)
# Update
# no. is immutable data type therefore we cant update or delete the no.

## DELETE
# del a,b,c # del flush out memory, delete variable
# print(a,b,c)

# Functions and methods
# abs()
# ceil()
# cmp()  # comapre-> not avaible in python version 3.X, available in 2.X
# exp()
# fabs()
# floor()
# log()
# log10()
# max()
# min()
# modf()
# pow()
# round()


##
# a=-23.34
# b=-24
# print(abs(a),abs(b)) # abs converts negative to positive
# import math as m    # because ceil is not a function its a method and for that we need to import the package
# print(m.ceil(a))
# print(m.exp(a)) #e^a
# print(m.exp(m.log(abs(a)))) # chaining -> inner func first executes
# print(m.log10(abs(a)))

# a=-23.34
# b=23.45
# c=23.5
# print(m.floor(a),m.floor(b),m.floor(c))
# print(m.fabs(a)) # returns floating absolute value
# print(abs(a))    # returns only absolute i.e. +ve value

# print(max(23,24,25,27,28,30)) # returns max. value
# print(min(5,67,123,1,3,54,65)) # returns min. value

# a=35.34
# print(m.modf(a)) # gives tupple form & segeregates real & decimal value

# a=23.456768
# print(round(a))
# print(round(a,2))

# a=3
# b=2
# print(m.sqrt(a))
# print(m.pow(a,b))


## Random package
# a="hello python"
# import random as r
# print(r.choice(a)) # gives random value

# a=list(range(1,100))
# print(r.choice(a))

# import random as r
# print(r.randrange(0,100,2))

# list1= list(range(1,10))
# r.shuffle(list1)
# print(list1)

# print(r.random()) # (fixed) gives floating no. b/w 0 & 1.

# print(r.uniform(10,30)) # floating no. b/w 10 & 30

## SEED
# print(r.random())
# r.seed(10)
# print(r.random())

# STRING
# a="hello_world"
# print(a[2])
# print(a[2:5])
# print(a[ :5])
# print(a[3: ])
# print(a[3:7:2])
# print(a[::1])
# print(a[::2])
# print(a[-10:9])
# print(a[-10:-3])
# print(a[ :-1])
# print(a[::-1])
# print(a[10:0:-1])
# print(a[::-2])
# print(a[-2:-9:-1])
# print(a[::1][::-1][::-2][::-3][::-1])
# NOTE:
# case 1: forward(start<end)
# case 2: backward/reverse(start>end)

# a[2]="hello" # doesnot support update becoz of immutability

## STRING FUNCTIONS AND METHODS 
# 1. cases:
# lower()
# upper()
# titlecase()
# swapcase()
# isupper()
# islower()
# istitlecase()

# 2. prefix & postfix:
# startswith()
# endswith()

# 3. design:
# center()
# ljust()
# rjust()

# 4. operations
# find()
# replace()
# count()
# index()
# len()

# 5. trimming:
# strip()
# lstrip()
# rstrip()

# 6. miscellaneous:
# join()
# split()
# isdigit()
# isspace()
# isalnum()


# a="hello world"
###############
# print(a.upper())
# print(a.lower())
# print(a.swapcase())
# print(a.isupper())
# print(a.islower())
###############

# print(a.center(20,"*"))
# print(a.ljust(20,"*"))
# print(a.rjust(20,"*"))
# print(len(a))

# a=input("enter your mobile no.")
# print(a[6:].rjust(len(a),"*"))

####################
# a=" hello world "
# print(a.startswith("h"))
# print(a.startswith("hello"))
# print(a.startswith("HELLO"))

# print(a.endswith("d"))
# print(a.endswith("world"))
# print(a.endswith("World"))

# print(a.find("World")) # if string doesnot found returns -1
# print(a.index("World")) # if string not found it raises exception
# and in count if string not found it returns 0

# print(a.replace("world","python"))
# print(a.replace("l","w",2))
# print(a[a.find("world"):].replace("l","w"))
# print(a[:6]+a[a.find("world"):].replace("l","w"))
# print(a[a.find("hello"):6].replace("o","e"))

####################
# print(a.strip())
# print(a.lstrip())
# print(a.rstrip())
# print(a.strip().replace(" ",""))

# a="hello world"
# data=a.split(" ")
# sep=" "
# print(data)
# print(sep.join(data))

# a="hello"
# print(a[0:].replace("e",""))

a= " "
print(a.isspace())
a= "1234abc"
print(a.isdigit())
print(a.isalnum())