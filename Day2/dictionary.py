# CRUD on dictionary
# CREATE
dict1= {
	"name":"abc",
	"std":"BE",
	"roll_no":34,
	"courses":["java","python"],
	"address":{"landmark":"lalbaug",
               "pincode":"400033"}
}
# print(dict1)

## READ
# print(dict1["std"])
# print(dict1["address"]["pincode"])  
# OR print(dict1["address"].get("pincode")) 
# OR print(dict1.get("address").get("pincode"))

## UPDATE       #dictionary is mutable therefore update and delete are possible
# dict1["name"]="xyz"
# print(dict1)

## DELETE
# del dict1["name"]
# print(dict1)



## DICTIONARY METHODS
# clear()
# copy()
# fromkeys()
# _contains_()
#has_key()

# get()
# setdefault()

# update()

# keys()
# values()
# items()

## FUNCTIONS AND METHODS
# dict1.clear()
# print(dict1)

# dict2=dict1
# dict3=dict1.copy() # copies original function
# print(dict1)
# print(dict2)
# print(dict3)

# dict1["name"]="rajesh"
# print(dict1)
# print(dict2)
# print(dict3)

#############
# list1=["name","std","roll_no","address"]
# print({}.fromkeys(list1,"NA"))
#############
# print(dict1.__contains__("name"))
#############
print(dict1.get("name"))
if(dict1.__contains__("name")):
	print(dict1["name"]=="raj")	
else:
	print("name not found")

print(dict1.setdefault("name"))
print(dict1)
##################
dict2={"contact_no.":"123456789",
		"address":"lalbaug"}
dict1.update(dict2)
print(dict1)